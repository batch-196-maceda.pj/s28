// db.users.insertOne({
// 
//     
// 
//     "username": "jennyBP",
// 
//     "password": "magandalangsiya123"
// 
//     
// 
// })

// db.users.insertOne({
// 
//     "username":"gukoSonGohan",
//     "password": "cellKiller"
// })
//multile docs at once.. database.collectionName.command()

// db.users.insertMany(
//     [
//         {               
//             "username":"pabloEscobar",
//             "password":"narcosking"
//         },
//         {
//             "username":"pedroEscobar",
//             "password":"carcosknight"
//         }
//      ]
//     )   
//         

// db.products.insertMany(
// [
//     {
//         "name":"gwapocologne",
//         "description":"Ang cologne na nakakagwapo!",
//         "price":150
//     },
//     {
//       "name":"gwaposabon",
//        "description":"Ang sabon na nakakagwapo!",
//        "price":120  
//      },
//     {
//        "name":"gwapo mirror",
//        "description":"Ang salamin na palagi kang gwapo!",
//        "price":500 
//      }
// ])

// db.users.find()
// 
// //db.Collection.find()- returns all documents in the selected collection.
// 
// db.users.find({"username":"pedroEscobar"})//this type of query fetches the specific string to find in a document,


// db.cars.insertMany(
//     [
//     {   
//         "name":"Vios",
//         "brand":"Toyota",
//         "type":"Sedan",
//         "price":1500000
//      },
//      {   
//         "name":"Tamaraw FX",
//         "brand":"Toyota",
//         "type":"AUV",
//         "price":750000
//      },
//      {   
//         "name":"City",
//         "brand":"Honda",
//         "type":"Sedan",
//         "price":1600000
//      }
//     ])
// 

// db.cars.find({"type":"Sedan"})
// db.cars.find({"brand":"Toyota"}).

//bd.dollectioname.findOne({}) finds and return the firs documen in the collection.
// db.cars.findOne({})

// this sample command line finds and return the firs item/document that matches the search criteria,
// db.cars.findOne({"type":"Sedan"})

//UpDATE
// db.users.updateOne({"username":"pedroEscobar"},{$set:{"username":"peterEscobar"}})//tageting updates

// db.users.updateOne({},{$set:{"username":"updateUsername"}})//targets the first document on the collection/.

// db.users.updateOne({"username":"pabloEscobar"},{$set:{"isAdmin":true}})//field is automatically added if not present. 


//db.users.updateMany({},{$set:{"isAdmin":true}})//this updates all document and adds the criteria to be added if not present.

//db.cars.updateMany({"type":"Sedan"},{$set:{"price":1000000}})//this update items that matches the criteria.


//DELETION

//db.products.deleteOne({})//deletes the first item on the collection if the criteria is empty.

//db.cars.deleteOne({"brand":"Toyota"}) //deletes the first item that matches the criteria.

//db.users.deleteMany({"isAdmin":true}) //deletes all documents who matches the criteria.

//db.collection.deleteMany({})//deletes all files in a collection

db.cars.deleteMany({})

db.products.deleteMany({})
