db.room.insertOne(
    {
        "name":"single",
        "accomodates":2,
        "price": 1000,
        "description":"A simple room with all the basic necessities",
        "rooms_available": 10,
        "isAvailable":false
     }
    )
     
db.room.insertMany(
        [
            {
                "name":"queen",
                "accomodates":4,
                "price": 4000,
                "description":"A room with a queen sized bed perferct for a simple getaway",
                "rooms_available": 15,
                "isAvailable":false   
             },
            {
                "name":"double",
                "accomodates":3,
                "price": 2000,
                "description":"A room fit for a small family  going on vactaion",
                "rooms_available": 5,
                "isAvailable":false
                }
            
         ])



db.room.find({"name":"double"})


db.room.updateOne({"name":"queen"},{$set:{"rooms_available":0}})

db.room.deleteMany({"rooms_available":0})